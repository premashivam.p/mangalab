import logging
import requests
import os
import re 

from django.conf import settings
from urllib.parse import urlparse



logger = logging.getLogger(__name__)

def extract_chapter_number(chapter_name: str) -> int:
    """
    Find the chapter number from the chapter name
    """
    pattern = r'(volume|chapter)\s*([0-9:]+)'
    matches = re.findall(pattern, chapter_name, flags=re.IGNORECASE)

    volume = None
    chapter = None

    for match in matches:
        keyword, value = match
        if keyword.lower() == 'volume':
            volume = int(re.sub(r'\W+', '', value))
        elif keyword.lower() == 'chapter':
            chapter = int(re.sub(r'\W+', '', value))
    try:
        return volume, chapter
    except Exception as e :
        logger.error(f"Error in extracting chapter number {e}")


def img_downloder(url:str, save_directory:str):
    """
    Download an image from the given URL and save it to the specified directory.
    
    Parameters:
        url (str): The URL of the image to download.
        save_directory (str): The directory where the image will be saved. Default is current directory.
        
    Returns:
        str: The file path of the downloaded image.
    """
    try:
        # Send a GET request to the URL
        response = requests.get(url)
        
        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            # Parse the URL to get the filename
            parsed_url = urlparse(url)
            filename = os.path.basename(parsed_url.path)
            
            # Save the image to the specified directory
            file_path = os.path.join(save_directory, filename)
            with open(file_path, "wb") as file:
                file.write(response.content)
                
            return True
        else:
            logger.info("Failed to download image. Status code:", response.status_code)
            return False
    except Exception as e:
        logger.error("An error occurred:", e)
        return False


def create_manga_structure(url:str, manga_title:str, chapter_no:str):
    """
    Create directory structure for storing manga files.

    Parameters:
        manga_title (str): Title of the manga.
        chapter_no (str): Name of the chapter.

    Returns:
        str: Path to the chapter directory.
    """
    base_dir = settings.BASE_DIR
    try:
        chapter_no = str(chapter_no)
    except Exception as _:
        chapter_no = f"{manga_title}_nill"

    # Directory for manga files
    manga_dir = os.path.join(base_dir, "Mangafiles")
    if not os.path.exists(manga_dir):
        os.makedirs(manga_dir)
        logger.info(f"Created directory: {manga_dir}")

    # Directory for manga title
    manga_title_dir = os.path.join(manga_dir, manga_title)
    if not os.path.exists(manga_title_dir):
        os.makedirs(manga_title_dir)
        logger.info(f"Created directory: {manga_title_dir}")

    # Directory for chapter
    chapter_dir = os.path.join(manga_title_dir, chapter_no)
    if not os.path.exists(chapter_dir):
        os.makedirs(chapter_dir)
        logger.info(f"Created directory: {chapter_dir}")
    
    res = img_downloder(url=url, save_directory=chapter_dir)
    if res:
        logger.info(f"img of {manga_title} for {chapter_no} downloded")

