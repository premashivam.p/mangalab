import logging
import scrapy
from scrapy import Request
from harvest.harvest.scraper_helper import model_helper as save_helper
from harvest.harvest.scraper_helper.scraper_helper import (
    extract_chapter_number, create_manga_structure)
from manga.models import MangaSource

logger = logging.getLogger(__name__)


class ManhwazSpider(scrapy.Spider):
    name = "manhwaz"
    allowed_domains = ["manhwaz.com"]
    start_urls = ["https://manhwaz.com/"]
    image_source = MangaSource.MANHWAZ

    def parse(self, response):
        manga_list = response.css("div.manga-content div.row.px-2.list-item div.col-6")

        for manga in manga_list:
            manga_title = manga.css('.item-summary .line-2 a::text').get()
            manga_url = manga.css('.item-summary .line-2 a::attr(href)').get()

            instance = save_helper.save_manga_list(data={
                'title': manga_title.strip(),
                'manga_url': manga_url.strip(),
            })

            if instance:
                yield Request(url=manga_url.strip(), callback=self.parse_manga,
                              meta={'instance': instance})
                logger.info("manga %s saved", instance.title)
            logger.info("Manga allredy updated")

    def parse_manga(self, response):
        instance = response.meta['instance']

        manga_desc = response.css("div.summary__content p::text").get()
        if manga_desc and not instance.description:
            instance.description = manga_desc
            instance.save()
        logger.info(f"description updated {instance.title}")

        manga_img = response.css("div.summary_image a img::attr(src)").get()
        data = {
            'manga': instance,
            'image': manga_img.strip(),
            'image_source': self.image_source,
        }
        save_helper.save_title_image(data=data)
        logger.info("image data saved %s", instance.title )

        chapter_list = response.css("div.list-chapter ul.list-item li.wp-manga-chapter")
        for chapter in chapter_list:
            chapter_url = chapter.css("a::attr(href)").get()
            chapter_name = chapter.css("a::text").get()

            volume, chapter_no = extract_chapter_number(chapter_name)
            if volume is None or volume == '':
                volume = 1

            try:
                save_helper.save_chapter_list(data={
                    'manga': instance,
                    'chapter_no': chapter_no,
                    'chapter_name': chapter_name.strip(),
                    'volume_no': volume,
                    'chapter_url': chapter_url.strip(),
                    'chapter_source': self.image_source,
                })
                # Downloding chapters
                yield Request(url=chapter_url.strip(), callback=self.parse_chapters,
                              meta={'manga_title': instance.title,
                                    'chapter_no': chapter_no})
            except Exception as e:
                logger.error(f"\n\nError: {e}\n\n")

            logger.info("Chapter %s saved for %s", chapter_name, instance.title)

    def parse_chapters(self, response):
        manga_title = response.meta['manga_title']
        chapter_no = response.meta['chapter_no']

        img_list = response.css("div#chapter_content")
        for img in img_list:
            img_url = img.css(".chapter-img img-responsive::attr(src)")
            if img_url:
                create_manga_structure(url=img_url, manga_title=manga_title,
                                   chapter_no=chapter_no)
            else:
                logger.error(f"no url")
            
